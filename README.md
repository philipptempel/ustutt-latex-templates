# UStutt LaTeX Templates

[![pipeline status](https://gitlab.com/philipptempel/ustutt-latex-templates/badges/master/pipeline.svg)](https://gitlab.com/philipptempel/ustutt-latex-templates/-/commits/master)
[![Last Release](https://img.shields.io/badge/dynamic/json.svg?label=Last%20release&url=https://gitlab.com/api/v4/projects/12280175/releases&query=0.released_at&colorB=brightgreen)]()
[![Stars](https://img.shields.io/badge/dynamic/json.svg?label=Stars&url=https://gitlab.com/api/v4/projects/12280175&query=star_count)]()
[![Forks](https://img.shields.io/badge/dynamic/json.svg?label=Forks&url=https://gitlab.com/api/v4/projects/12280175&query=forks_count&colorB=important)]()
[![Issues](https://img.shields.io/badge/dynamic/json.svg?label=Open%20issues&url=https://gitlab.com/api/v4/projects/12280175/issues_statistics&query=statistics.counts.opened&colorB=critical)]()
[![License](https://img.shields.io/badge/dynamic/json.svg?label=License&url=https://gitlab.com/api/v4/projects/12280175?license=true&query=license.key&colorB=yellow)]()
[![License](https://img.shields.io/badge/dynamic/json.svg?label=License&url=https://gitlab.com/api/v4/projects/12280175?license=true&query=license.name&colorB=yellow)]()
[![made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg)](https://github.com/iswunistuttgart/latex-templates)
[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://github.com/iswunistuttgart/latex-templates/graphs/commit-activity)
[![Last activity](https://img.shields.io/badge/dynamic/json.svg?label=Last%20activity&url=https://gitlab.com/api/v4/projects/12280175&query=last_activity_at&colorB=informational)]()

This is the nonofficial repository of LaTeX document classes, styles, packages, and templates for documents at the University of Stuttgart.
If you are interested in just finding the most recent files you need for writing code, please refer to the releases page:

[Download most recent release](https://github.com/iswunistuttgart/latex-templates)

## Requirements

### LaTeX Distribution

You need an up-to-date version of a LaTeX distribution on your current operating system.
We generally advise **against** MikTeX, and advise **for** using [TeXlive](https://www.tug.org/texlive/).

### Compiler Engine

As the trend in development of LaTeX related packages and classes is to move away from `pdflatex` towards `xelatex` or `lualatex`, we are not providing support for `pdflatex` anymore.
As such, you will need to compile your document using either `XeLaTeX` or `luaLaTeX` (preferably `luaLaTeX`, as the classes are more thoroughly tested against `luaLaTeX`).

Additionally, if you want to play it safe, use `latexmk` to build your document. Our provided `.latexmkrc` files include all the necessary commands to compile everything in the right order. Only requirement is access to a command line with `latexmk` on your path. Some LaTeX-IDEs also allow using `latexmk` instead of some weird `lualatex + makeindex + ... + dvips` combo.

PS: The developers are solely using `latexmk`, so support for users using `latexmk` will be the best.

## Usage

If you download the files from the respective released archives (article, book, or thesis), you have all needed files as well as a template file. Simply open the contained `.tex` file and get going.

## Contributing

Before sending a pull request, be sure to check out the [Contribution Guidelines](CONTRIBUTING.md) first.

Our repository hosted on our public [GitHub repo](http://github.com/iswunistuttgart/latex-templates) is a mirror of the original code hosted on our [local GitLab repo](https://git.isw.uni-stuttgart.de/projekte/eigenentwicklungen/templates/latex/). Any member (staff, student) of the University of Stuttgart can request access to the repo, fork it, and create a merge request.

## Development Team

UStutt LaTeX Templates was created by [Philipp Tempel](http://www.isw.uni-stuttgart.de/institut/mitarbeiter/Tempel/) who both uses and continues developing the templates.

* [Philipp Tempel](http://philipptempel.de)
* Christoph Hinze
