SHELL = /bin/bash

DISTDIR ?= dist/
INSTALLDIR ?= $(shell kpsewhich --var-value TEXMFHOME)/tex/latex/ustutt/
SOURCES ?= $(shell find ./ -type f -name "ustutt*.dtx")
DICTIONARIES ?= ustutt-English.dict ustutt-German.dict
ADLL_INSTALL ?= tikzlibraryustutt.code.tex ipa-authoryear.bbx ipa-authoryear.cbx $(DICTIONARIES)
ADDL_DIST ?= .latexmkrc references.bib images acronyms.tex symbols.tex notation.tex
DOCS ?= $(SOURCES:dtx=pdf)

LATEX ?= pdflatex --shell-escape
MAKEINDEX ?= makeindex -s
LATEXMK ?= latexmk

# default goal: install for user
.DEFAULT_GOAL := install

# all targets
.PHONY: all
all: ins dist

# install i.e., turn `dtx` file into `cls`, `sty`, or `tex` files
.PHONY: ins
ins: ustutt.ins $(SOURCES)
	$(LATEX) ustutt.ins

# build docs for all `cls` and `sty` files
.PHONY: docs
docs: ins $(DOCS)

# any PDF file depends on its base documented TeX file
%.pdf: %.dtx
	$(LATEX) $^
	$(MAKEINDEX) gind.ist $*.idx
	$(MAKEINDEX) gglo.ist -o $*.gls $*.glo
	$(LATEX) $^

# class files depend on the installer
%.cls: ins

# combination of everything that needs to be done for distributing
.PHONY:
dist: ins docs artcl book theses

# building `artcl` class
.PHONY: article artcl
article:
	$(MAKE) artcl
artcl:
	$(MAKE) ins
	$(MAKE) publish_artcl
	$(MAKE) build_artcl
publish_artcl: ins
	$(MAKE) publish_regular_artcl
build_artcl: publish_artcl
	$(MAKE) build_document_artcl

# building `book` class
.PHONY: book publish_book build_book
book:
	$(MAKE) ins
	$(MAKE) publish_book
	$(MAKE) build_book
publish_book: ins
	$(MAKE) publish_regular_book
build_book: publish_book
	$(MAKE) build_document_book

# building all theses
.PHONY: theses
theses: bachelor master doctorate

# building thesis `bachelor`
.PHONY: bachelor publish_bachelor build_bachelor
bachelor:
	$(MAKE) ins
	$(MAKE) publish_bachelor
	$(MAKE) build_bachelor
publish_bachelor: ins
	$(MAKE) publish_thesis_bachelor
build_bachelor: publish_bachelor
	$(MAKE) build_document_bachelor

# building thesis `master`
.PHONY: master publish_master build_master
master:
	$(MAKE) ins
	$(MAKE) publish_master
	$(MAKE) build_master
publish_master: ins
	$(MAKE) publish_thesis_master
build_master: publish_master
	$(MAKE) build_document_master

# building thesis `doctorate`/`phd`
.PHONY: doctorate publish_doctorate build_doctorate
doctorate:
	$(MAKE) ins
	$(MAKE) publish_doctorate
	$(MAKE) build_doctorate
publish_doctorate: ins
	$(MAKE) publish_thesis_doctorate
build_doctorate: publish_doctorate
	$(MAKE) build_document_doctorate

publish_thesis_%: ins
	# create the directory
	mkdir -p $(DISTDIR)/$*
	# copy class file over
	cp ustuttthesis.cls $(DISTDIR)/$*/
	# copy all style files over
	cp *.sty $(DISTDIR)/$*/
	# copy source files over
	cp $*_*.tex $(DISTDIR)/$*/
	# copy additional includes over
	cp -r $(ADLL_INSTALL) $(DISTDIR)/$*/
	cp -r $(ADDL_DIST) $(DISTDIR)/$*/

# publish regular documents
publish_regular_%: ins
	# create the directory
	mkdir -p $(DISTDIR)/$*
	# copy class file over
	cp ustutt$*.cls $(DISTDIR)/$*/
	# copy all style files over
	cp *.sty $(DISTDIR)/$*/
	# copy source files over
	cp $*_*.tex $(DISTDIR)/$*/
	# copy additional includes over
	cp -r $(ADLL_INSTALL) $(DISTDIR)/$*/
	cp -r $(ADDL_DIST) $(DISTDIR)/$*/

# build any document
build_document_%:
	$(LATEXMK) -cd $(DISTDIR)/$*/$*_*.tex
	# and remove auxiliary files from the dist directory
	$(LATEXMK) -c -cd $(DISTDIR)/$*/$*_*.tex

# make release
.PHONY: release
release:
	$(MAKE) release_package_artcl
	$(MAKE) release_package_book
	$(MAKE) release_package_bachelor
	$(MAKE) release_package_master
	$(MAKE) release_package_doctorate

# release a single target
release_package_%:
	cd $(DISTDIR) && zip --recurse-paths --no-dir-entries "../$*.zip" -- "$*/"
	cd $(DISTDIR) && tar -cvzf "../$*.tar.gz" -- "$*/"

# clean directory from all dirt
.PHONY: clean
clean:
	[ `ls -1 *.cls 2>/dev/null | wc -l` == 0 ] || rm *.cls
	[ `ls -1 *.sty 2>/dev/null | wc -l` == 0 ] || rm *.sty
	[ `ls -1 *.dict 2>/dev/null | wc -l` == 0 ] || rm *.dict
	[ `ls -1 *.tex 2>/dev/null | wc -l` == 0 ] || $(LATEXMK) -c -silent *.tex
	[ `ls -1 *.dtx 2>/dev/null | wc -l` == 0 ] || $(LATEXMK) -c -silent *.dtx
	[ `ls -1 *.dtx 2>/dev/null | wc -l` == 0 ] || $(LATEXMK) -c -silent *.ins

# reseat directory to its original, distributed state
.PHONY: distclean
distclean: clean
	[ `ls -1 *.tex 2>/dev/null | wc -l` == 0 ] || $(LATEXMK) -C -silent *.tex && rm -f *.tex
	[ `ls -1 *.dtx 2>/dev/null | wc -l` == 0 ] || $(LATEXMK) -C -silent *.dtx
	[ `ls -1 *.dtx 2>/dev/null | wc -l` == 0 ] || $(LATEXMK) -C -silent *.ins
	[ ! -d $(DISTDIR) ] || rm -r $(DISTDIR)

# copy compiled files over to user's texmf home
.PHONY: install
install: ins
	mkdir -p $(INSTALLDIR)
	cp *.cls $(INSTALLDIR)
	cp *.sty $(INSTALLDIR)
	cp *.dict $(INSTALLDIR)
	cp -r $(ADLL_INSTALL) $(INSTALLDIR)

# uninstall from user's texmf home
.PHONY: uninstall
uninstall: distclean
	rm -rf $(INSTALLDIR)
